import React, { useState } from 'react'
//匯入MyDisplay.js
import MyDisplay from '../tmp-0603-02/components/MyDisplay'
//匯入MyButton.js
import MyButton from '../tmp-0603-02/components/MyButton'

function App() {
  const [total, setTotal] = useState(0)

  return (
    //JSX語法:<自定義function component>
    <>
      <MyDisplay total={total} />
      <MyButton
        title="+1"
        clickMethod={() => {
          setTotal(total + 1)
        }}
      />
      <MyButton
        title="-1"
        clickMethod={() => {
          setTotal(total - 1)
        }}
      />
    </>
  )
}

export default App