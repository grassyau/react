import React, { Fragment } from 'react'
import { useState } from 'react'

function App() {
  const [total, setTotal] = useState(999)
  return (
    <Fragment>
      <h1>{total}</h1>
      <button
        onClick={() => {
          setTotal(total + 1)
        }}
      >
        +
      </button>
      <button
        onClick={() => {
          setTotal(total - 1)
        }}
      >
        -
      </button>
    </Fragment>
  )
}

export default App
